# CI/CD mit Gitlab
  
Die Lektüre und das schlichte Begreifen dieses Exkurses benötigen etwa zwei Stunden Zeit. Wer es für sich nachstellen und vielleicht auch dokumentieren möchte, benötigt mindestens fünf Stunden.  
  
**Inhaltsverzeichnis**  
- [CI/CD mit Gitlab](#cicd-mit-gitlab)
  - [Was ist GitLab-CI/CD?](#was-ist-gitlab-cicd)
    - [GitLab-CI/CD-Pipeline](#gitlab-cicd-pipeline)
    - [GitLab-CI/CD-Architektur](#gitlab-cicd-architektur)
      - [GitLab-Projekt](#gitlab-projekt)
      - [GitLab-Runner](#gitlab-runner)
      - [Deploy-Server](#deploy-server)
  - [Das Demo-Projekt](#das-demo-projekt)
    - [Übersicht](#übersicht)
    - [git und Authorisierung](#git-und-authorisierung)
    - [Clone](#clone)
    - [Tests](#tests)
    - [Konfiguration der Pipeline](#konfiguration-der-pipeline)
      - [Erster Job: run\_tests](#erster-job-run_tests)
      - [Zweiter Job: build\_image](#zweiter-job-build_image)
      - [Stages](#stages)
      - [Dritter Job: Deployment](#dritter-job-deployment)
    - [Docker-Basics](#docker-basics)

GitLab bietet für den technologischen Ansatz CI/CD schon alles, was man für einen soliden Start braucht.  
Zur Erinnerung: CI/CD steht für Continuous Integration und Continuous Deployment. Dies hat jetzt erstmal noch nichts mit Kubernetes zu tun, bildet aber die Grundlage für den gewählten Ansatz, CI/CD mit Kubernetes zu verbinden.  
Grundlage für diesen Kurs ist ein [Youtube Tutorial aus Nana Janashias Vlog WorldTech](https://youtu.be/qP8kir2GUgo)  
Nähere Details mit einer Beschreibung aller Keywords stehen in der [CI/CD-Doku von GitLabs](https://docs.gitlab.com/ee/ci/)  
  
Wir wollen also mit GitLabs eine komplette CI/CD-Pipeline erstellen, die aus  
  * einer fertigen [Python-App](README_APP.md) als Ausgangspunkt besteht,
  * diverse Tests auf Funktionalität vornimmt,  
  * ein Docker-Image erstellt,  
  * dieses in ein privates Repository auf Docker-Hub pusht
  * und dieses Image auf unserem Cluster-Server deployt.  
  
Anhand dieses Beispiels wollen wir verstehen,  
  * wie unsere Workflow-Pipeline aussieht,  
  * wie die GitLab-Architektur aussieht und  
  * wie wir die Konzepte und Möglichkeiten von GitLab-CI/CD anwenden können.  
  
## Was ist GitLab-CI/CD?
![Gitlabs-CI/CD](images/gitlab_ci-cd.png)  
Die Entwickler von GitLab sind fokussiert, GitLab zu einer umfassenden Anwendung für den gesamten CI/CD-Workflow zu machen.  
Zu diesem Zweck wurden die nötigen Werkzeuge zum
  * Planen  
  * Entwickeln  
  * Testen  
  * Packages erstellen  
  * Releases verwalten und  
  * Überwachen  
  
bereits realisiert und stehen uns "out-of-the-box" zur Verfügung.  
  
### GitLab-CI/CD-Pipeline
CI/CD steht für 
  * Continuous Integration und  
  * Continuous Deployment oder Continuous Delivery  
  
Damit ist gemeint, dass **automatisch und kontinuierlich fortlaufend** im Prozess die folgenden Dinge aus- bzw. durchgeführt werden:  
  * Das Erstellen und Bearbeiten von Code.  
  * Das Testen dieses Codes.  
  * Das Erzeugen der nötigen Komponenten (typischerweise Container-Images).  
  * Das Speichern der Images im Hub.
  * Das Bereitstellen der Container.  
  
Wenn also ein Entwickler oder eine Entwicklerin im gemeinsamen GitLab-Git eine Änderung am Code durchführt, werden automatisch von der GitLap-Software alle diese Prozesse ("_Pipeline_") durchlaufen. So werden fortlaufend Code-Änderungen am Release an die abschließende Umgebung durchgereicht und dabei verarbeitet.  
Hierbei steht **CI** für die Prozessstufen  
  * Codeänderung und  
  * Codetests  
  
und schließt mit dem Upload des Images ins Repository ab.  
**CD** führt dann fort mit den Deployments der Images in die Umgebungen, die typischerweise lauten:  
  * Develop  
  * Staging  
  * Produktion  
  
Der Vorteil von GitLab-CI/CD im Vergleich zu anderen Lösungen wie z.B. Jenkins ist hier, dass Git und Pipeline sich hier nahtlos zusammenfügen.  

### GitLab-CI/CD-Architektur  
#### GitLab-Projekt
Zur GitLab-CI/CD-Architektur gehört zunächst einmal eine _GitLab-Instanz_ mit einem _Projekt_. (Alternativ auch ein eigener GitLab-Server.) In diesem Projekt werden    
  * der Code,  
  * die Pipelinekonfiguration und  
  * die GitLabkonfiguration bereitgehalten, sowie  
  * die Pipelineausführung gesteuert.  
  
#### GitLab-Runner
Die ausführenden Einheiten in der Architektur nennen sich _GitLab Runners_.  
Es handelt sich dabei um sogenannte Agents, also kleine durchgängig laufende Dienste auf Servern, die den Code in der Instanz überwachen und die geforderten Aktionen (Pipeline-Jobs) ausführen.  
Im Standard-Setup einer GitLab-Instanz sind alle nötigen Runners bereits aktiviert und einsatzbereit.  
(Bei eigenen GitLab-Servern muss das ggf. noch bereitgestellt werden.)  

#### Deploy-Server
Das Deployment soll auf einem meiner virtuellen Debian-Server ausgerollt werden. Dieser Server hat z.B. den Domainnamen ```vs.demoserver.de```.  
Zu diesem Server gibt es ssh-Zugriff mit public keys.  
Damit nicht mein Hauptschlüsselpaar für diese Verbindung verwendet werden muss, erstelle ich ein weiteres Paar nur für diesen Zweck. Hintergrund: Der _private key_ dieses Paares muss später im GitLab hinterlegt werden.  
Im Terminal meines Macs führe ich aus:
```console
cd ~
ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa): .ssh/gitlab_key
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in .ssh/gitlab_key
Your public key has been saved in .ssh/gitlab_key.pub
The key fingerprint is:
SHA256:8/KKaUijvaMyFde7rT...1A9ZnY6FTy7zMVBtJM fh@MBP.local
The key's randomart image is:
+---[RSA 3072]----+
|           .o    |
|         . . o   |
|     .    + E    |
|  . . ..   = o   |
|   o  ..S + o    |
|  . o..+ X o     |
| . +.o+o+ =      |
|o . +=+B.+       |
| oo+=+=+=o.      |
+----[SHA256]-----+
-------------------------------------------------------------
```
Eine _passphrase_ wird nicht eingegeben.  
Wir kopieren den _public key_ und hinterlegen diesen auf dem Deploymentserver:
```console
cat .ssh/gitlab_key.pub 
ssh-rsa AAAAB3NzaC1yc2EAAA...
...
...1THdl6ZGZIkU= fh@MBP.local

ssh root@vs.demoserver.de
nano .ssh/authorized_keys
```
Nun den kopierten _public key_ einfügen, die Datei mit _ctrl O_ sichern und sich aus dem System mit _exit_ ausloggen.
```console
ssh -i .ssh/gitlab_key root@vs.demoserver.de
...
Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: ...
root@vs:~#
```
Wir können uns also jetzt als root auf dem Server mittels gitlab_key-Paar einloggen.
Aber damit Container auf diesem Server ausgeführt werden können, muss docker installiert sein.  
```console
docker
Command 'docker' not found.
```
Also schnell noch installieren:
```console
apt-get update && apt-get install docker.io
systemctl start docker
systemctl enable docker
```
Testaufruf mit
```console
docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
exit
```
Docker ist installiert, aber ein Container läuft noch nicht.

## Das Demo-Projekt  
### Übersicht  
Unser Demo-Projekt soll eine kleine (fertige) Python-App zum Leben erwecken.  
Es besteht aus  
  * der App (Python),  
  * Tests in der Pipeline,  
  * dem Build des Images,  
  * dem Push in die Docker-Registry und  
  * dem Deployment zum Server  
  
und soll ein kleines Werkzeug zur Darstellung der Serverauslastung visualisieren.  
Das zugehörige GitLab-Repo ist hier im Ordner [ci-cd-mit-gitlab](ci-cd-mit-gitlab/README.md) eingebunden.  
Der Python-Code wird hier einfach so eingebunden. Python-Kenntnisse sind nicht vonnöten. Es handelt sich um ein Fork des Repos [python-demoapp](https://github.com/benc-uk/python-demoapp) von Ben Coleman, benk-uk.  

### git und Authorisierung
Zuerst muss das Repo auf den Server geklont werden. Dazu verbinden wir uns in der Shell per SSH als root zum Server. Dort muss das Debian-Paket git installiert sein. Wenn nicht, dann dies mit  
```console
apt-get update && apt-get install git -y
```
installieren.  
Außerdem muss der Server auf das GitLab-Repo zugreifen dürfen. Dazu muss auf dem Server ein SSH-Schlüsselpaar vorhanden sein. Dies liegt im Ordner .ssh/ im Benutzerverzeichnis.  
In diesem Tool soll es einfach bleiben. Deswegen arbeiten wir auf dem Server als root. Der absolute Pfad des .ssh-Ordners ist also /root/.ssh/  
Und wir befinden uns in /root/  
Das Erzeugen des Schlüsselpaares gelingt in unserer Shell über  
```console
ssh-keygen -t ed25519 -C "comment"
```
Der Kommentar ist dabei frei zu wählen. Es kann z.B. eine Domain des Servers sein. Der vorgeschlagene Pfad sollte bei der Erstellung bestätigt werden. Ein Passwort sollte nicht vergeben werden.  
  
Anschließend kann der öffentliche Schlüssel des Paares aufgerufen werden:
```console
cat ~/.ssh/id_ed25519.pub
```
Er hat eine solche Form:  
```console
ssh-ed25519 AAAA<...>E0as2hN1 <comment>
```
Diesen Schlüssel bitte in die Zwischenablage kopieren und im GitLab-Repo einsetzen.  
Das geht im GitLab-Benutzerprofil unter den Einstellungen (Preferences) im Menüpunkt _SSH-Schlüssel_ (SSH Keys).  Dort wird der Schlüssel eingetragen. Das Titelfeld füllt sich normalerweise automatisch mit dem Kommentar und das Ablaufdatum kann nach eigenen Wünsche gewählt werden. *Zum Schluss mit Klick auf _Schlüssel hinzufügen_ (Add key) den Schlüssel im GitLab-Repo hinterlegen. Dieses vertraut daraufhin den Zugriffen unseres Servers.  
Wer möchte, kann das überprüfen mit einem SSH-Zugiff vom Server auf das GitLab-Repo:
```console
ssh -T git@gitlab.com
> Welcome to GitLab, @<Name>!
```  
### Clone
Ich habe das Projekt von Jana geforkt, so dass wir es klonen und in den Projektordner wechseln können:  
```console
git clone git@gitlab.com:Talpaworld/ci-cd-mit-gitlab
cd ci-cd-mit-gitlab/
ls -al
```
Wir sehen u.a. einen Ordner _tests/_. Dort enthalten ist ein Python-Script, das die Konfiguration für die Testroutinen enthält.  
```
-rw-r--r-- 1 root root 2795  6. Mär 08:59 CONTRIBUTING.md
drwxr-xr-x 3 root root 4096  6. Mär 08:59 deploy
drwxr-xr-x 2 root root 4096  6. Mär 08:59 .devcontainer
-rw-r--r-- 1 root root  398  6. Mär 08:59 Dockerfile
-rw-r--r-- 1 root root   26  6. Mär 08:59 .dockerignore
-rw-r--r-- 1 root root   28  6. Mär 08:59 .flake8
drwxr-xr-x 5 root root 4096  6. Mär 08:59 .github
-rw-r--r-- 1 root root 1353  6. Mär 08:59 .gitignore
-rw-r--r-- 1 root root  901  6. Mär 08:59 .gitlab-ci.yml
-rw-r--r-- 1 root root 1056  6. Mär 08:59 LICENSE
-rw-r--r-- 1 root root 3059  6. Mär 08:59 makefile
-rw-r--r-- 1 root root  114  6. Mär 08:59 .prettierrc.yaml
-rw-r--r-- 1 root root 5437  6. Mär 08:59 README.md
drwxr-xr-x 3 root root 4096  6. Mär 08:59 src
drwxr-xr-x 2 root root 4096  6. Mär 08:59 tests
drwxr-xr-x 2 root root 4096  6. Mär 08:59 .vscode
```
Im Ordner _src/app/tests/_ sind die Testroutinen enthalten.   
Die Logik und Funktionsweise der Tests müssen wir an dieser Stelle nicht verstehen. Es reicht erstmal, sich diese Abläufe zu vergegenwärtigen.  
In diesem speziellen Beispiel haben wir zudem noch eine Datei namens _makefile_. Dies ist eine spezielle Datei für den Befehl _make_, die Anweisungen (z.B. test) für diese Umgebung enthält.  

### Tests
Um die Tests durchlaufen zu lassen, rufen wir im Terminal
```console
make test
```
auf und stellen dabei vermutlich fest, dass der Server eine Fehlermeldung ausgibt:  
```console
-bash: make: Kommando nicht gefunden.
```
Wir können also noch nicht testen, weil der Server den Befehl _make_ noch nicht kennt.  
Natürlich lässt sich _make_ einfach mit 
```console
apt-get update && apt-get install make -y
```
installieren, aber wir werden dies mit in die Pipeline einbauen, damit das auf debianartigen Systemen immer funktioniert, egal, ob _make_ bereits installiert ist, oder noch nicht.  
  
### Konfiguration der Pipeline
Gemäß dem CI/CD-Ansatz besteht natürlich auch unsere Pipeline "nur" aus Code.  
GitLab fordert dafür eine spezielle [YAML-Datei](../dateiformate/YAML.md) _.gitlab-ci.yml_, die wir auch in unserem Repo finden. In dieser Datei ist die gesamte CI/CD-Konfiguration beschrieben.  
Durch diese Vorgabe kann GitLab die CI/CD-Pipeline ohne weitere Definitionen und Zuordnungen ausführen.  
In unserem Repo befindet sich diese Datei bereits in fertigen Zustand. Deswegen nennen wir diese um und legen dann eine neue Datei im root-Verzeichnis des Gits an. (Ich arbeite am liebsten mit dem Editor _nano_. Es geht aber natürlich auch mit anderen wie _vim_.)  
```console
mv .gitlab-ci.yml _.gitlab-ci.yml
nano .gitlab-ci.yml
```
Die einzelnen Schritte in unserer YAML-Datei _.gitlab-ci.yml_ werden _jobs_ genannt. Wir legen also in dieser Datei einen _job_ für jeden Schritt an. Und jeder Job bekommt zunächst mal einen Namen.  

#### Erster Job: run_tests  
Unser erster Job  heißt **run_tests**. Abschließen tut den Jobnamen ein Doppelpunkt.  
(**Nicht** als Jobnamen verwendet werden dürfen: after_script, before_script, cache, false,  image, include, nil, services, stages, true, types und variables.)  
Dem Jobnamen folgen dann die Attribute oder Konfigurationen zum Job. Dabei auf die Einrückungen achten (YAML!!).  
Ein Wert des Jobs definiert den Script hinter _script:_. Dies kann ein Array von Scriptaufrufen sein. In unserem Fall, wie schon oben versucht: _make test_.  
Damit der Test mit _make_ aber vollzogen werden kann, wird die Installation von _make_ benötigt. Außerdem der Pythoninterpreter _python_ in Verbindung mit _pip_, dem Package-Installer für Python, damit unser Python-Script die benötigten Routinen finden und nachladen kann. Um möglichst plattformunabhängig zu sein. installieren wir diese Funktionen als Container. Auf diese Weise muss nur Docker auf dem System installiert sein und jeder Prozess läuft dann isoliert in seinem Container.  
Die Gitlab-Runners stellen per Default einen ruby-Container beret. Wir benötigen aber hier einen Container mit Python und PiP. Deswegen müssen wir vor dem Scriptaufruf noch das Containerimages bestimmen.  
Einen passenden Container finden wir im [Docker-Hub](https:hub.docker.com). In unserem Fall wählen wir das Image _python_ in der Version _3.9-slim-buster_.  
Wer sum ersten Ma im Docker-Hub ist, registriert sich bitte direkt und legt sich so einen Account an. Benötigen wir dann später.  
Nun sieht unsere Datei _.gitlab-ci.yml_ inzwischen so aus:
```
run_tests:
  image: python:3.9-slim-buster
  script:
    - make test
```
Was uns noch fehlt, ist _make_. Wir installieren deswegen **in** den Python-Container _make_.  
Dazu gibt es das Attribut _before\_script_.  
```
run_tests:
  image: python:3.9-slim-buster
  before_script:
    - apt-get update && apt-get install make -y
  script:
    - make test
```
Damit haben wir, wenn auch eine sehr kleine, also nur mit einem Job versehene, Pipeline, die wir ausführen können.  
Da wir die Datei in unserem Repo-Klon erstellt haben, müssen wir sie zuerst _committen_ und dann ins Git _pushen_.  
```console
git commit -a
git push
```
Unmittelbar nachdem Push der Datei _.gitlab-ci.yml_ startet bzw. aktiviert GitLab die Pipeline und die im Job _run_tests_ geforderten Schritte werden durchgeführt. Es wird also zuerst der Python-Container initialisiert und dorthinein _make_ installiert. Dann werden die Testroutinen durchlaufen.  
Können wir das irgendwo sehen? Ja. Schauen wir ins Projekt auf gitlab.com:  
![Menü _CI/CD --> Pipeline_](images/gitlab_ci-cd_pipeline.png)  
In unserem Fall sehen wir: Tests bestanden ("passed"). Also wurden die vorgeschalteten Aufgaben auch erfüllt.  
Ein Klick auf die Pipeline-ID öffnet einen Dialog, in dem wir den Job der Pipeline erneut durchlaufen lassen können.  
  
Wähle ich links unter _CI/CD_ den Menüpunkt _Jobs_ erhalte ich diese Übersicht:  
  
![Menü _CI/CD --> Jobs](images/gitlab_ci-cd_jobs.png)  
  
Ein Klick auf die Job-ID zeigt das Logfile des Jobs:  
```
...
collecting ... collected 4 items  
src/app/tests/test_api.py::test_api_monitor PASSED                       [ 25%]  
src/app/tests/test_views.py::test_home PASSED                            [ 50%]  
src/app/tests/test_views.py::test_page_content PASSED                    [ 75%]  
src/app/tests/test_views.py::test_info PASSED                            [100%]  
============================== 4 passed in 2.32s ===============================  
Cleaning up project directory and file based variables 00:00  
Job succeeded  
```
Am Ende sehen wir: Alle vier Tests sind erfolgreich durchlaufen.  
Aber zwischendurch gibt es auch ein  
```
make: git: Command not found
```
Wir können zur Installation von make noch git hinzufügen, damit das auch im Container vorhanden ist.
Dazu müssen wir gar nicht mehr den Editor auf dem Server aufrufen. Das geht auch bequem direkt im Projekt im Menü _CI/CD --> Editor_. Hier wird direkt die Pipeline-Konfigurationsdatei _.gitlab_ci.yml_ aufgerufen. Und wir machen aus  
```
    - apt-get update && apt-get install make -y
```
```
    - apt-get update && apt-get install make git -y
```
Im Anschluss klicken wir unten auch _Commit changes_.  
Schauen wir uns das Log des Jobs an, dann sehen wir, dass die Fehlermeldung verschwunden ist.  
Noch eine andere Unschönheit gibt es: "WARNING: You are using pip version 22.0.4; however, version 23.0.1 is available."  
Wir verwenden ein Image mit einem old-stable Debian (Buster). Aktuell ist derzeit Bullsey. Versuchen wir es also doch mal mit dem Image und vielleicht einer passenden Python-Version. Im Dockerhub finden wir da z.B. 3.11-slim-bullseye. Also tragen wir doch im Job dieses Image ein (python:3.11-slim-bullseye) und commiten erneut.  
Wir ernten einen Fehler, weil etwas im Python-Code dieser Demo-App mit deser neueren Python-Version nicht mehr kompatibel 
ist. Das neuere Betriebssystem scheint aber keine Fehler zu verursachen. Also nehmen wir die ältere Python-Version 3.9 (Image: python:3.9-slim-bullseye) und leben mit der Warnung auf das veraltete pip.  
  
Wir sehen hier aber schön, wie einfach es ist, durch die Änderung der Tags beim Image das Image zu tauschen. Ein Klick auf _Commit_ und die Pipeline wird erneut durchlaufen.  
  
Unsere Tests sind also erfolgreich durchgelaufen und die weiteren Änderungen dieser Konfiguration führen wir nun hier im Editor durch. Das ist einfacher.  
  
#### Zweiter Job: build_image  
Damit ein Container gestartet werden kann, muss zuerst ein Containerimage erstellt werden.  
Typischerweise wird das Image nicht mit dem System generiert, auf dem es später auch laufen soll. Deswegen müssen wir es in ein Container-Repository schieben, von dem aus das Ziel-System es wieder laden und dann starten kann.  
Es gibt verschiedene solcher Container-Repos. Das bekannteste ist sicher das Docker-Hub. Wenn bei Docker-Verbindungsaufrufen kein Repo angegeben wird, dann wird immer das Docker-Hub angesprochen.  
Aber auch unsere GitLab-Umgebung hat ein Docker-Repo, in das wir das Image ablegen können.  
Das Procedere ist dabei im Prinzip identisch. Ich beschreibe mal exemplarisch beide Verfahren.  

##### Verwendung des Docker-Hubs als Container-Repository  
Um Images ins Docker-Hub ablegen zu können, legen wir uns in unserem [Docker-Hub Konto](https://hub.docker.com) ein Repository an:  
  
![Docker-Hub Repository](images/gitlab_ci-cd_docker-hub.png)  
  
Unser Repo auf Docker-Hub soll private sein. Das Setting dazu kann im Repo oben im Menüpunkt "Settings" vorgenommen werden.  
Um ein Image in ein privates Repository zu pushen, werden Zugangsdaten ("docker login...") benötigt. Diese Zugangsdaten sind Username und Password des Docker-Hub-Kontos
  
##### Alternativ: Verwenden unseres GitLab-Repos als Container-Repository  
Nehmen wir das GitLab-Repo auch zur Ablage der Containerimages, dann müssen wir dazu kein Konto und kein Repository erstellen. Es ist immer schon vorhanden. Im Menü links unter _Packages and registries --> Container-Registry_ bekommt man angezeigt, welche Befehle zum Login, Builden und Pushen benötigt werden. Diese können wir direkt so übernehmen und fügen nur noch den Image-Namen an.  
Im GitLab-Hub melden wir uns aber nicht mit einen Paar User/Password an, sondern mit User/Token. Dieses Token muss zunächst erstellt werden.  
Dazu rechts oben auf das eigene Icon klicken und das _Profil bearbeiten_. Links _Zugangs-Token_ (Authentication-Token) wählen. Dann dem neuen Token einen Namen geben, ggf. ein Ablaufdatum und den _api-Scope_ aktivieren. Das Token mit Mausklick erstellen. Es wird dann oben verdeckt angezeigt. Das Token ins Clipboard übernehmen. Sicherheitshalber auch notieren.  
  
##### Zugangsdaten sicher speichern.
Damit der Gitlab-Runner das Image im Repoa ablegen und später auch wieder auf den anderen Server laden kann, müssen wir diese Daten in der Pipeline-Definition verwenden.  
Natürlich ist es vollkommen unzulässig, diese Zugangsdaten direkt in die YAML-Date zu schreiben. Wir realisieren diese Parameterübergabe verschlüsselt über sogenannte _Secrets_. Dazu rufen wir im GitLab-Menü unten die _Einstellungen (Settings) --> CI/CD_ auf und öffnen den Punkt _Variables_.  
Wir fügen also zwei Key-Value-Paare ein:  
  1. _CONTAINERREPO\_USER_ -> <Docker-Hub Username>  (Setze Flags "Protect" und "Mask Variable")  
  2. _CONTAINERREPO\_PASS_ -> <Docker-Hub Password>  (Setze Flags "Protect" und "Mask Variable")  
Verwenden wir das GitLab-Repo, dann verwenden wir Username und das zuvor erstellte Token:  
  1. _CONTAINERREPO\_USER_ -> <GitLab Username>  (Setze Flags "Protect" und "Mask Variable")  
  2. _CONTAINERREPO\_PASS_ -> <GitLab Token>  (Setze Flags "Protect" und "Mask Variable")  
  
Die hier eingegebenen Variablen sind unmittelbar in unserer Pipeline-Definition _.gitlabs_ci.yml_ zu verwenden.  
  
##### Job erstellen  
Im GitLab-Menü _CI/CD --> Editor_ erstellen wir den Job _build\_image_ und verwenden die zuvor erstellten Variablen in _before\_script_. Zuerst die Variante, wenn das Docker-Hub verwendet wird:    
```
run_tests:
  image: python:3.9-slim-bullseye
  before_script:
    - apt-get update && apt-get install make git -y
  script:
    - make test

    
build_image:
  variables:
    IMAGE_NAME: talpaworld/gitlab_ci-cd_demo-app
    IMAGE_TAG: python-app-1.0
  before_script:
    - docker login -u $CONTAINERREPO_USER -p $CONTAINERREPO_PASS
  script:
    - docker build -t $IMAGE_NAME:$IMAGE_TAG .
    - docker push $IMAGE_NAME:$IMAGE_TAG
```  
Bei der Verwendung des GitLab-Container-Repos muss noch die Adresse des Repos mit angegeben werden:
```
...
build_image:
  variables:
    IMAGE_NAME: talpaworld/gitlab_ci-cd_demo-app
    IMAGE_TAG: python-app-1.0
    CONTAINERREPO_NAME: registry.gitlab.com/talpaworld/ci-cd-mit-gitlab
  before_script:
    - docker login -u $CONTAINERREPO_USER -p $CONTAINERREPO_PASS $CONTAINERREPO_NAME
  script:
    - docker build -t $CONTAINERREPO_NAME/$IMAGE_NAME:$IMAGE_TAG .
    - docker push $CONTAINERREPO_NAME/$IMAGE_NAME:$IMAGE_TAG
```  
  
Im Script wird das Containerimage erstellt und ins Container-Repo gepusht.  
  
Wir verwenden hier auch Variablen. Wenn diese nur im Job _build\_image_ verwendet werden sollen, dann ist die Position wie hier oben angegeben korrekt. Werden die Variablen in der ganzen Datei benötigt, müssen sie oberhalb der Jobs definiert werden:  
```
variables:
  IMAGE_NAME: talpaworld/gitlab_ci-cd_demo-app
  IMAGE_TAG: python-app-1.0
  # Bei GitLab als Container-Repo noch:
  CONTAINERREPO_NAME: registry.gitlab.com/talpaworld/ci-cd-mit-gitlab

run_tests:
  image: python:3.9-slim-bullseye
  before_script:
    - apt-get update && apt-get install make git -y
  script:
    - make test

    
build_image:
  before_script:
...
```  
  
Soweit sieht unser Job _build\_image_ nicht schlecht aus. Das Problem aber ist, dass der GitLab-Runner mit dem Befehl _docker_ so wenig anzufangen weiß, wie zuvor mit dem Befehl _make_ ohne unsere Installation mittels apt-get. Was können wir tun?  
Da unsere Umgebung komplett dockerbasiert ist, benötigen wir ein Image für einen Docker-Container mit docker: **docker in docker**.  
Wir schauen also wieder im Docker-Hub, suchen nach "docker" und finden z.B. [Docker in Docker](https://hub.docker.com/_/docker).  Wir wählen einen der jüngsten Tags und ergänzen unsere Job-Beschreibung damit.  
```
...
build_image:
  image: docker:23.0.1
  before_script:
    - docker login ...
...
```
Neben dem Container mit dem Docker-Client ("job-container") benötigen wir aber auch noch den Container mit dem Docker-Deamon ("service-container").  
```
...
build_image:
  image: docker:23.0.1
  services:
    - docker:23.0.1-dind
  before_script:
    - docker login ...
...
```  
"docker:23.0.1-dind" ist der Service-Container, auf den der Job-Container zugreifen kann. "dind" steht für _docker-in-docker_.  
Damit die beiden Container miteinder kommunizieren können, müssen wir Docker anweisen, ein Zertifikat zu erstellen und den beiden Containern zuzuweisen. Dies geschieht über eine bestimmte Variable (DOCKER_TLS_CERTDIR).  
```
...
build_image:
  image: docker:23.0.1
  services:
    - docker:23.0.1-dind
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  before_script:
    - docker login ...
...
```  
Zusammengefasst wird in unserem Job _build\_image_ in den ersten Zeilen eine Docker-Umgebung erstellt, die dann in den weiteren Zeilen verwendet wird, um zuerst die Anmeldung durchzuführen, dann das Image zu erstellen und in das Container-Repo (Docker-Hub oder GitLab) zu pushen.  
  
Wir commiten die Datei und schauen uns die Pipeline an:  
  
![Pipeline mit zwei Jobs](images/gitlab_ci-cd_build-image.png)  
  
Wir stellen fest: Die beiden Jobs laufen offensichtlich parallel und nicht nacheinander ab. Zum Schluss laufen aber beide Jobs erfolgreich durch:  
  
![Beide Jobs erfolgreich](images/gitlab_ci-cd_build-image_success.png)  
  
Wir finden deswegen das Image auch in unserem Docker-Hub-Repo wieder:  
  
![Die Tags in unserem Docker-Hub-Repository](images/gitlab_ci-cd_build-image_docker.png)  
  
Wer GitLab als Container-Repo nutzt, schaut unter _Packages and registry --> Container-Registry_:  

![Die Tags in unserem GitLab-Container-Repo](images/gitlab_ci-cd_containerrepo-gitlab.png)  
  
#### Stages  
Um einen zeitlich kontrollierten Ablauf in der Pipeline zu gewährleisten gibt es Stages-Attribute. Damit lassen sich einzelne oder mehrere Jobs einem Ablauf-Zustand zuordnen, so dass sie koordiniert nacheinander oder auch tw. parallel bearbeitet werden. 
In unserem Beispiel also:  
  1. Stage 1: run_tests  
  2. Stage 2: build_image  
  3. Stage 3: deploy  
   
*Anmerkung: Im Folgenden wird nur noch das Script für die Verwendung mit Docker-Hub als Container-Repo gezeigt. Der Ansatz mit GitLab läuft analog. Das fertige Script wird am Schluss gelistet.*  
  
Nur wenn ein Abschnitt ("Stage") mit allen ihm zugeordneten Jobs erfolgreich durchlaufen ist, soll mit dem nächsten fortgefahren werden.  
```
variables:
  IMAGE_NAME: talpaworld/gitlab_ci-cd_demo-app
  IMAGE_TAG: python-app-1.0

stages:
  - test
  - build

run_tests:
  stage: test
  image: python:3.9-slim-bullseye
  before_script:
    - apt-get update && apt-get install make git -y
  script:
    - make test


build_image:
  stage: build
  image: docker:23.0.1
  services:
    - docker:23.0.1-dind
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  before_script:
    - docker login -u $CONTAINERREPO_USER -p $CONTAINERREPO_PASS
  script:
    - docker build -t $IMAGE_NAME:$IMAGE_TAG .
    - docker push $IMAGE_NAME:$IMAGE_TAG
```
Auf der Hauptebene werden also die Stages definiert, die dann den Jobs zugeordnet werden. Dabei können auch mehrere Jobs in einem Stage sein. In diesem Fall laufen die parallel ab.  
  
Wir können diese Pipelinedefinition commiten und uns den Ablauf in der Pipeline ansehen:  
  
![Ablauf in Phasen (Stages)](images/gitlab_ci-cd_stages.png)  
  
Während die erste Phase ausgeführt wird, steht die zweite wartend an. Nach ca. 2 Minuten sollten beide Phasen einen grünen Haken haben. Im Docker-Hub muss dann das aktualisierte Image zu sehen sein.  

#### Dritter Job: Deployment  
Um das Deployment durchführen zu können, muss der Gitlab-Runner auf den Deployments-Server zugreifen können. Wir legen deswegen den _private key_ des Paares, dessen _public key_ wir schon auf dem Deployserver abgelegt hatten, als Secret im Git ab.  
Zuerst kopieren wir uns den _private key_ (mit BEGIN- und END-Comment):
```console
cat ~/.ssh/gitlab_key
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABlwAAAAdzc2gtcn
...
sD1xOcpDaF3c8AAAAMZmhATUJQLmxvY2FsAQIDBAUGBw==
-----END OPENSSH PRIVATE KEY-----
```
Dann erstellen wir die Secret-Variable im GitLab-Menü _Einstellungen (Settings) --> CI/CD --> Variables_.  
Wir nennen Sie "SSH_KEY" und kopieren den Schlüssel hinein. Den _Type_ bestimmen wir als _File_. An das Ende des Keys geben wir noch eine Leerzeile ein.  
  
![Konfiguration SSH_KEY-Variable in GitLab-CI/CD](images/gitlab_ci-cd_ssh-key.png)  
  
_Variable hinzufügen_ nicht vergessen!  
Anschließend wechseln wir wieder in den CI/CD-Pipeline-Editor.  
Wir fügen zunächst einen Stage _deploy_ ein und dann einen neuen Job _deploy:_ und sortieren diesen dem Stage _deploy_ zu.  
```console
variables:
  IMAGE_NAME: talpaworld/gitlab_ci-cd_demo-app
  IMAGE_TAG: python-app-1.0

stages:
  - test
  - build
  - deploy

run_tests:
  stage: test
  image: python:3.9-slim-bullseye
  before_script:
    - apt-get update && apt-get install make git -y
  script:
    - make test


build_image:
  stage: build
  image: docker:23.0.1
  services:
    - docker:23.0.1-dind
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  before_script:
    - docker login -u $CONTAINERREPO_USER -p $CONTAINERREPO_PASS
  script:
    - docker build -t $IMAGE_NAME:$IMAGE_TAG .
    - docker push $IMAGE_NAME:$IMAGE_TAG


deploy: 
  stage: deploy
  before_script:
    - chmod 400 $SSH_KEY
  script:
    - ssh -o StrictHostKeyChecking=no -i $SSH_KEY root@11.cluster.collox.de "
        docker login -u $CONTAINERREPO_USER -p $CONTAINERREPO_PASS && 
        docker ps -aq | xargs docker stop | xargs docker rm && 
        docker run -d -p 5000:5000 $IMAGE_NAME:$IMAGE_TAG"
```  
  
Hier die Fassung des Scripts für den Fall, dass GitLab als Container-Repo verwendet wird:
```console
variables:
  CONTAINERREPO_NAME: registry.gitlab.com/talpaworld/ci-cd-mit-gitlab
  IMAGE_NAME: gitlab_ci-cd_demo-app
  IMAGE_TAG: python-app-1.0

stages:
  - test
  - build
  - deploy

run_tests:
  stage: test
  image: python:3.9-slim-bullseye
  before_script:
    - apt-get update && apt-get install make git -y
  script:
    - make test


build_image:
  stage: build
  image: docker:23.0.1
  services:
    - docker:23.0.1-dind
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  before_script:
    #- docker login -u $DOCKERHUB_USER -p $DOCKERHUB_PASS
    - docker login -u $CONTAINERREPO_USER -p $CONTAINERREPO_PASS $CONTAINERREPO_NAME
  script:
    #- docker build -t $IMAGE_NAME:$IMAGE_TAG .
    - docker build -t $CONTAINERREPO_NAME/$IMAGE_NAME:$IMAGE_TAG .
    #- docker push $IMAGE_NAME:$IMAGE_TAG
    - docker push $CONTAINERREPO_NAME/$IMAGE_NAME:$IMAGE_TAG


deploy:
  stage: deploy
  before_script:
    - chmod 400 $SSH_KEY
  script:
    #- ssh -o StrictHostKeyChecking=no -i $SSH_KEY root@11.cluster.collox.de "
    #    docker login -u $DOCKERHUB_USER -p $DOCKERHUB_PASS && 
    #    docker ps -aq | xargs -r docker stop | xargs -r docker rm && 
    #    docker run -d -p 5000:5000 $IMAGE_NAME:$IMAGE_TAG"
    - ssh -o StrictHostKeyChecking=no -i $SSH_KEY root@11.cluster.collox.de "
        docker login -u $CONTAINERREPO_USER -p $CONTAINERREPO_PASS $CONTAINERREPO_NAME && 
        docker ps -aq | xargs -r docker stop | xargs -r docker rm && 
        docker run -d -p 5000:5000 $CONTAINERREPO_NAME/$IMAGE_NAME:$IMAGE_TAG"
```
Im Detail:
  1. before_script: Die Datei mit dem private key bekommt nur für den Owner Schreib- und Leserecht. GitLab fasst die Rechte per Default weiter. Deswegen muss das umgestellt werden.  
  2. Die SSH-Verbindung findet ohne Keycheck ("Known Hosts") statt, weil nicht händisch eingegriffen werden kann.  
  3. Die SSH-Verbindung findet mit dem private key für GitLab statt, den wir bei den CI/CD-Variablen hinterlegt haben.  
  4. Docker-Login im Container-Repo (Docker-Hub oder GitLab), um das Image zu pullen.  
  5.1. Alle laufenden Docker-Container anzeigen. Marker -q bedeutet: Es werden nur die Container-IDs angezeigt und in xargs gelistet.  
  5.2. Alle Container, deren ID in der xargs-Liste steht, werden gestoppt  
  5.3. und gelöscht. Marker -r bedeutet, dass dies nicht ausgeführt wird, wenn die Liste in xargs leer ist.
  6. Das unter build_image erstellte und gepushte Image wird gepullt und aktiviert.  

Ein Klick auf "Commit changes" startet die Pipeline.  
Wir schauen gespannt.  
  
![Job run_tests aktiv](images/gitlab_ci-cd_pipeline-deploy_1.png)  
  
![Job build_image aktiv](images/gitlab_ci-cd_pipeline-deploy_2.png)  
  
![Job deploy aktiv](images/gitlab_ci-cd_pipeline-deploy_3.png)  
  
![Alles fertig. Deployment erfolgt.](images/gitlab_ci-cd_pipeline-deploy_4.png)  
  
Dann schauen wir doch mal, ob der Container läuft:  
```console
ssh -i ~/.ssh/gitlab_key root@vs.demoserver.de
docker ps
CONTAINER ID   IMAGE                                             COMMAND                  CREATED         STATUS         PORTS                    NAMES
18b038a29931   talpaworld/gitlab_ci-cd_demo-app:python-app-1.0   "gunicorn -b 0.0.0.0…"   6 minutes ago   Up 6 minutes   0.0.0.0:5000->5000/tcp   loving_tu
```
Sieht gut aus.  
Und funktioniert der Container auch? Wir probieren es aus  
[http://vs.demoserver.de:5000/](http://vs.demoserver.de:5000/)  
Voilá!  
  
Anmerkung: Mit  
```console
docker rm 18b038a29931
docker rm 18b038a29931
``` 
können wir den Container wieder entfernen. Aber die Pipeline läuft noch! Wenn irgendetwas im Git neu commitet wird, wird der Container erneut erstellt.  
  
### Docker-Basics
Zum Schluss noch eine Hand voll Docker-Befehle zum Umgang mit Containern:
| Befehl | Erklärung |
|--------|-----------|
| docker ps | Listet alle laufenden Container. Mit Argument -a werden alle vorhandenen Container genannt|
| docker stop <id> | Stoppt den Container mit der ID <id> |
| docker start <id> | Startet den Container mit der ID <id> |
| docker rm <id> | Entfernt den Conatiner mit der ID <id> |
| docker exec <id> | Führt einen Befehl im Container aus. Ein ```docker exec -it <id> sh``` startet z.B. eine Shell im Container. (Wenn auf dem Container vorhanden, kann man auch bash statt sh verwenden.) |